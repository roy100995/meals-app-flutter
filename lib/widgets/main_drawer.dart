import 'package:expense_app/screens/filters_screen.dart';
import 'package:flutter/material.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        child: Column(
          children: [
            Container(
              height: 70.0,
              width: double.infinity,
              padding: EdgeInsets.all(10.0),
              color: Theme.of(context).accentColor,
              alignment: Alignment.centerLeft,
              child: Text(
                'Cooking up!',
                style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 28.0,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            buildListTile(
              'Meals',
              Icons.restaurant,
              context,
              () {
                Navigator.of(context).pushReplacementNamed('/');
              },
            ),
            buildListTile(
              'Filters',
              Icons.settings,
              context,
              () {
                Navigator.of(context)
                    .pushReplacementNamed(FiltersScreen.routeName);
              },
            ),
          ],
        ),
      ),
    );
  }

  ListTile buildListTile(
    String title,
    IconData icon,
    BuildContext context,
    Function onTabHandler,
  ) {
    return ListTile(
      leading: Icon(
        icon,
        color: Theme.of(context).primaryColor,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24.0,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: onTabHandler,
    );
  }
}
